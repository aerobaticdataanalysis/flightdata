from flightdata.fields import Fields
from flightdata.mapping.inav import inav_io_info
from flightdata.mapping.ardupilot import ardupilot_io_info
import numpy as np
import pandas as pd
import unittest
from inav_log_reader import Parser
from ardupilot_log_reader.reader import Ardupilot
import math
from typing import List, Dict
from importlib.util import find_spec



class Flight(object):
    def __init__(self):
        self._data = None
        self._playbackindex = 0
        self._frame_rate_estimate = None
        self._parser = None

    @property
    def data(self):
        if self._data is None:
            self._data = pd.DataFrame(columns=Fields.all_names())
        return self._data

    @data.setter
    def data(self, value):
        self._data = value
        self._data.index = self._data.index - self._data.index[0]

    @staticmethod
    def from_data(_data, fr_estimate=None, parser = None):
        _flight = Flight()
        _flight.data = _data
        _flight._frame_rate_estimate = fr_estimate
        _flight._parser = parser
        return _flight

    @staticmethod
    def read_inav(log_file):
        _parser = Parser()
        _parser.load_log(log_file)

        _data = pd.DataFrame(
            columns=inav_io_info.base_names, 
            data=_parser.read_specific_array(inav_io_info.io_names, inav_io_info.factors_to_base)
        )
        
        _data = _data[1:].interpolate(
            method='linear',
            limit_direction='backward'
        )
        _data.index = _data[Fields.TIME.names[0]].copy()
        _data.index.name = 'time_index'
        return Flight.from_data(
            _data.loc[pd.isna(_data['time_actual'])==False], 
            parser=_parser
            )
    
    @staticmethod
    def read_ardupilot(log_file):
        _parser = Ardupilot(log_file, read_types = ['AETR', 'AHR2', 'ARSP', 'ATT', 'BARO', 'CTUN', 'GPS', 'IMU', 
        'IMU2', 'MAV', 'NTUN', 'PIDP', 'PIDR', 'PIDY', 'PM', 'POS', 'RCIN', 'RCOU', 'BAT', 'MODE', 'MAVC', 'NKF1' ])
        fulldf = _parser.full_df()
        _data = fulldf.get(list(set(fulldf.columns.to_list()) & set(ardupilot_io_info.io_names)))

        _fewer_io_info = ardupilot_io_info.subset(_data.columns.to_list())
        
        _data = _data * _fewer_io_info.factors_to_base
        _data.columns = _fewer_io_info.base_names

        missing_cols = pd.DataFrame(
            columns=list(set(Fields.all_names()) - set(_data.columns.to_list()))+[Fields.TIME.names[0]]
            )

        _data = _data.merge(missing_cols, on=Fields.TIME.names[0], how='left')
        _data.index = _data[Fields.TIME.names[0]].copy()
        _data.index.name = 'time_index'
        return Flight.from_data(
            _data.loc[pd.isna(_data['time_actual'])==False], 
            parser=_parser
            )

    def read_fields(self, fields):
        return self.data[Fields.some_names(fields)] 

    @property
    def duration(self):
        return self.data.tail(1).index.item()
    
    @property
    def frame_rate_estimate(self):
        if not self._frame_rate_estimate:
            self._frame_rate_estimate =  self.duration / len(self.data)
        return self._frame_rate_estimate

    def read_row_by_id(self, names, index):
        return list(map(self.data.iloc[index].to_dict().get, names))
    
    def read_closest(self, names, time):
        """Get the row closest to the requested time.
        
        :param names: list of columns to return
        :param time: desired time in microseconds
        :return: dict[column names, values]
        """
        return self.read_row_by_id(names, self.data.index.get_loc(time, method='nearest'))
    
    @property
    def column_names(self):
        return self.data.columns.to_list()

    def _slice(self, start_time=0, end_time=-1):
        if start_time == 0 and end_time == -1:
            new_data = self.data
        elif start_time == 0:
            end = self.data.index.get_loc(end_time, method='nearest')
            new_data = self.data.iloc[:end]
        elif end_time == -1:
            start = self.data.index.get_loc(start_time, method='nearest')
            new_data = self.data.iloc[start:]
        else:
            start = self.data.index.get_loc(start_time, method='nearest')
            end = self.data.index.get_loc(end_time, method='nearest')
            new_data = self.data.iloc[start:end]
        
        return Flight.from_data(new_data, fr_estimate=self.frame_rate_estimate)
    
    def read_slice(self, names=None, start_time=0, end_time=-1):
        """get a section of the data between the specified times, for the specified columns
        
        :param names: list of columns to retrieve
        :param start_time:
        :param end_time:
        :return: dataframe of requested slice
        """
        if not names:
            return self._slice(start_time, end_time)
        else:
            return self._slice(start_time, end_time)[names]
    
    def read_array(self, columns: List[str], transpose=False):
        """Get a list of lists containing the entire dataset for the specified columns.
        
        :param columns: list of columns being requested.
        :param transpose: bool, if True, outer list is columns, if False, outer list is rows
        :return: List[List[float]] the data for those columns.
        """
        if transpose:
            return self.data[columns].transpose().values.tolist()
        else:
            return self.data[columns].values.tolist()
           

class TestFlightData(unittest.TestCase):
    def setUp(self):
        self._flight = Flight.read_inav('./test/LOG00001.TXT')
    
    def test_framerate_estimate(self):
        self.assertAlmostEqual(self._flight.frame_rate_estimate, 0.03219621820)

    def test_read_closest(self):        
        self.assertEqual(self._flight.read_closest([Fields.TIME.names[0]], 0)[0], 20.823304 )

    def test_read_slice(self):
        slice1  = self._flight.read_slice( start_time=10, end_time=12)
        self.assertAlmostEqual(slice1.duration, 2.0, places=1)

    def test_read_ardupilot(self):
        _flight = Flight.read_ardupilot('./test/00000054.BIN')
        self.assertEqual(len(_flight.data), 150379)


    #"/mnt/c/Users/thoma/Documents/Mission Planner/logs/FIXED_WING/1/2020-05-08 18-06-04.bin"

if __name__ == "__main__":
    _flight = Flight.read_ardupilot("test/00000054.BIN")
    pass