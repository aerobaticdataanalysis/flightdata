"""
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""


from flightdata.fields import Fields, MappedField, FieldIOInfo
from pint import UnitRegistry

ureg = UnitRegistry()

# this maps the inav log variables to the tool variables
# ref https://github.com/iNavFlight/inav/wiki/INAV-blackbox-variables
log_field_map = dict()

#TIME = Field('time', ureg.second, 2, names=['flight', 'actual'])
#log_field_map["loopIteration"] = MappedField(Fields.LOOPITERATION, 0, "loopIteration", 1)
log_field_map["time"] = MappedField(Fields.TIME, 0, "time", ureg.second / 1000000)
#log_field_map["time"] = MappedField(Fields.TIME, 1, "time", ureg.microsecond) 

#TXCONTROLS = Field('tx_controls', ureg.second, 6, description='PWM Values coming from the TX')
#SERVOS = Field('servos', ureg.second, 8, description='PWN Values going to the Servos')
log_field_map["rcData[0]"] = MappedField(Fields.TXCONTROLS, 0, "rcData", ureg.second / 1000000)
log_field_map["rcData[1]"] = MappedField(Fields.TXCONTROLS, 1, "rcData", ureg.second / 1000000)
log_field_map["rcData[2]"] = MappedField(Fields.TXCONTROLS, 2, "rcData", ureg.second / 1000000)
log_field_map["rcData[3]"] = MappedField(Fields.TXCONTROLS, 3, "rcData", ureg.second / 1000000)
log_field_map["rcCommand[0]"] = MappedField(Fields.TXCONTROLS, 4, "rcCommand", ureg.second / 1000000)
log_field_map["rcCommand[1]"] = MappedField(Fields.TXCONTROLS, 5, "rcCommand", ureg.second / 1000000)
log_field_map["rcCommand[2]"] = MappedField(Fields.TXCONTROLS, 6, "rcCommand", ureg.second / 1000000)
log_field_map["rcCommand[3]"] = MappedField(Fields.TXCONTROLS, 7, "rcCommand", ureg.second / 1000000)

#FLIGHTMODE = Field('mode', 1, 1, description='The active flight mode ID')
log_field_map["flightModeFlags"] = MappedField(Fields.FLIGHTMODE, 0, "flightModeFlags", 1)

#EKFPOSITION = Field('ekf_position', ureg.radians, 3, description='position of plane in cartesian coordinates (s, e, u)', names=['x', 'y', 'z'])
log_field_map["navPos[0]"] = MappedField(Fields.EKFPOSITION, 0, "navPos", ureg.centimeter * -1)
log_field_map["navPos[1]"] = MappedField(Fields.EKFPOSITION, 1, "navPos", ureg.centimeter)
log_field_map["navPos[2]"] = MappedField(Fields.EKFPOSITION, 2, "navPos", ureg.centimeter)

#GLOBALPOSITION = Field('global_position', ureg.degrees, 3, names=['latitude', 'longitude', 'altitude'])
log_field_map["GPS_coord[0]"] = MappedField(Fields.GLOBALPOSITION, 0, "GPS_coord", ureg.degree / 10000000)
log_field_map["GPS_coord[1]"] = MappedField(Fields.GLOBALPOSITION, 1, "GPS_coord", ureg.degree / 10000000)

#SENSORALTITUDE = Field('altitude', ureg.meters, 2, names=['gps', 'baro'])
log_field_map["GPS_altitude"] = MappedField(Fields.SENSORALTITUDE, 0, "GPS_altitude", ureg.meter)
log_field_map["BaroAlt"] = MappedField(Fields.SENSORALTITUDE, 1, "BaroAlt", ureg.centimeter)

log_field_map["GPS_numSat"] = MappedField(Fields.GPSSATCOUNT, 0, "GPS_numSat", 1)

#ATTITUDE = Field('attitude', ureg.radian, 3, description='euler angles, order = yaw, pitch, roll', names=['roll', 'pitch', 'yaw'])
log_field_map["attitude[0]"] = MappedField(Fields.EKFATTITUDE, 0, "attitude", ureg.degree / -10)
log_field_map["attitude[1]"] = MappedField(Fields.EKFATTITUDE, 1, "attitude", ureg.degree / -10)
log_field_map["attitude[2]"] = MappedField(Fields.EKFATTITUDE, 2, "attitude", ureg.degree / -10)

#BATTERY = Field('battery', ureg.volt, 2, description='battery voltages')
log_field_map["vbat"] = MappedField(Fields.BATTERY, 0, "vbat", ureg.centiV)
log_field_map["sagCompensatedVBat"] = MappedField(Fields.BATTERY, 1, "sagCompensatedVBat", ureg.centiV)


log_field_map["axisRate[0]"] = MappedField(Fields.AXISRATE, 0, "IMUGyrX", ureg.degree / ureg.second)
log_field_map["axisRate[1]"] = MappedField(Fields.AXISRATE, 1, "IMUGyrY", ureg.degree / ureg.second)
log_field_map["axisRate[2]"] = MappedField(Fields.AXISRATE, 2, "IMUGyrZ", ureg.degree / ureg.second)


inav_io_info = FieldIOInfo(log_field_map)

if __name__ == "__main__":
    print(inav_io_info.base_names)
    print(inav_io_info.io_names)
    print(inav_io_info.factors_to_base)
    print(inav_io_info.factors_to_field)