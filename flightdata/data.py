"""
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""
import unittest
from typing import List, Dict
import numpy as np
import pandas as pd
from importlib.util import find_spec
from enum import Enum

from inav_log_reader import Parser
from ardupilot_log_reader.reader import Ardupilot
from geometry import GPSPosition

from flightdata.fields import Fields
from flightdata.mapping.ardupilot_ekfv2 import ardupilot_ekfv2_io_info
from flightdata.mapping.ardupilot_ekfv3 import ardupilot_ekfv3_io_info
from flightdata.mapping.inav import inav_io_info
from flightdata.flightline import FlightLine, Box


class LogType(Enum):
    INAV = 0
    ARDUPILOT = 1


def _read_inav(log_file: str) -> (Parser, pd.DataFrame):
    _parser = Parser()
    _parser.load_log(log_file)

    _data = pd.DataFrame(
        columns=inav_io_info.base_names, 
        data=_parser.read_specific_array(inav_io_info.io_names, inav_io_info.factors_to_base)
    )

    _data = _data[1:].interpolate(
        method='linear',
        limit_direction='backward'
    )
    _data.index = _data[Fields.TIME.names[0]].copy()
    _data.index.name = 'time_index'

    return _parser, _data


def _read_ardupilot(log_file: str) -> (Ardupilot, pd.DataFrame):
    _field_request = ['ARSP', 'ATT', 'BARO', 'GPS', 'IMU', 'RCIN', 'RCOU', 'BAT', 'MODE', 'NKF1', 'STAT', 'XKF1']
    _parser = Ardupilot(log_file, types = _field_request, zero_time_base=True)

    ekfv = _parser.parms['AHRS_EKF_TYPE']        
    print(ekfv)
    if ekfv==2:
        ardupilot_io_info = ardupilot_ekfv2_io_info
    elif ekfv==3:
        ardupilot_io_info = ardupilot_ekfv3_io_info
    else:
        raise IOError('unknown EKF type')
    
    fulldf = _parser.join_logs(_field_request)

    _data = fulldf.get(list(set(fulldf.columns.to_list()) & set(ardupilot_io_info.io_names)))

    _fewer_io_info = ardupilot_io_info.subset(_data.columns.to_list())
    
    _data = _data * _fewer_io_info.factors_to_base
    _data.columns = _fewer_io_info.base_names
    
    missing_cols = pd.DataFrame(
        columns=list(set(Fields.all_names()) - set(_data.columns.to_list()))+[Fields.TIME.names[0]]
        )

    _data = _data.merge(missing_cols, on=Fields.TIME.names[0], how='left')
    _data.index = _data[Fields.TIME.names[0]].copy()
    _data.index.name = 'time_index'
    return _parser, _data.loc[pd.isna(_data['time_actual'])==False]


class Flight(object):
    def __init__(self, data, box: Box=None, origin: GPSPosition = None, parameters: List = None, zero_time_offset: float = 0):
        self.data = data
        self.parameters = parameters
        self.zero_time = self.data.index[0] + zero_time_offset
        self.data.index = self.data.index - self.data.index[0]
        self._origin = origin

        if box is not None:
            self._set_box(
                FlightLine(
                    Box('home', self.origin, GPSPosition(self.origin.latitude - 0.1, self.origin.longitude)),
                    box
                ))

    @staticmethod
    def from_log(log_path, box=None, log_type: LogType = LogType.ARDUPILOT):
        if log_type == LogType.ARDUPILOT:
            _parser, data = _read_ardupilot(log_path)
        elif log_type == LogType.INAV:
            _parser, data = _read_inav(log_path)
        return Flight(data, box)


    @property
    def duration(self):
        return self.data.tail(1).index.item()
    
    def read_row_by_id(self, names, index):
        return list(map(self.data.iloc[index].to_dict().get, names))
    
    def read_closest(self, names, time):
        """Get the row closest to the requested time.
        
        :param names: list of columns to return
        :param time: desired time in microseconds
        :return: dict[column names, values]
        """
        return self.read_row_by_id(names, self.data.index.get_loc(time, method='nearest'))
    
    @property
    def column_names(self):
        return self.data.columns.to_list()

    def read_fields(self, fields):
        return self.data[Fields.some_names(fields)]

    @property
    def origin(self) -> GPSPosition:
        '''this is the GPS position where EKFPosition is zero. '''
        if not self._origin:
            gpsdata = self.read_fields([Fields.GLOBALPOSITION, Fields.GPSSATCOUNT])
            gpsdata = gpsdata.loc[pd.isna(gpsdata.iloc[:,0])==False]

            origin = gpsdata.loc[gpsdata.iloc[:,2] > 5].iloc[0] 
            self._origin = GPSPosition(origin['global_position_latitude'], origin['global_position_longitude'])
        return self._origin

    def _set_box(self, flightline: FlightLine):
        self._rotate_vals(Fields.EKFPOSITION.names, Fields.POSITION.names, flightline.field_point_to_box)
        self._rotate_vals(Fields.EKFATTITUDE.names, Fields.ATTITUDE.names, flightline.field_euler_to_box)
    
    def _rotate_vals(self, in_names, out_names, method):

        rotated_vals = pd.DataFrame(
            method(
                self.data[in_names[0]],
                self.data[in_names[1]],
                self.data[in_names[2]])
            ).transpose()

        rotated_vals.index = self.data.index
        rotated_vals.columns = out_names

        self.data[out_names] = rotated_vals[out_names]

    @staticmethod
    def section(flight, start_time: float, end_time: float):

        if start_time == 0 and end_time == -1:
            new_data = flight.data
        elif start_time == 0:
            end = flight.data.index.get_loc(end_time, method='nearest')
            new_data = flight.data.iloc[:end]
        elif end_time == -1:
            start = flight.data.index.get_loc(start_time, method='nearest')
            new_data = flight.data.iloc[start:]
        else:
            start = flight.data.index.get_loc(start_time, method='nearest')
            end = flight.data.index.get_loc(end_time, method='nearest')
            new_data = flight.data.iloc[start:end]

        return Flight(
            data=new_data, 
            origin=flight.origin, 
            parameters=flight.parameters, 
            zero_time_offset=flight.zero_time
            )


class TestFlightData(unittest.TestCase):
    def setUp(self):
        self._flight = _flight2 = Flight.from_log('test/ekfv3_test.BIN', Box.from_f3a_zone_file('test/gordano_box.f3a'), log_type=LogType.ARDUPILOT)
    
    def test_duration(self):
        self.assertAlmostEqual(self._flight.duration, 278.923721)

    def test_read_closest(self):
        self.assertAlmostEqual(self._flight.read_closest(Fields.TIME.names, 220)[0] - self._flight.zero_time, 220, 0)

    def test_slice(self):
        short_flight = Flight.section(self._flight, 100, 200)
        self.assertAlmostEqual(short_flight.duration, 100, 0)


if __name__ == "__main__":

    Flight.from_log('/home/tom/Desktop/logs/20200911/00000042.BIN', Box.from_f3a_zone_file('test/gordano_box.f3a'), log_type=LogType.ARDUPILOT)
  
    #unittest.main()